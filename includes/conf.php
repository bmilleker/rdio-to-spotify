<?php
    define("SPOTIFY_CLIENT_ID", "");
    define("SPOTIFY_CLIENT_SECRET", ""); 
    define("SPOTIFY_ACCESS_TOKEN", "");
    define("SPOTIFY_AUTH_URL", "http://somedomain.local/spotify-auth.php");

    define("RDIO_CLIENT_ID", "");
    define("RDIO_CLIENT_SECRET", ""); 
    define("RDIO_ACCESS_TOKEN", "");
    define("RDIO_AUTH_URL", "http://somedomain.local/rdio-auth.php");

    define("LIBRARY_CSV", "data/library.csv");
?>