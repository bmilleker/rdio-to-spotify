<?php
    include_once("includes/conf.php");  
    
    if (empty($_GET['code'])) {
        $auth_url = "https://accounts.spotify.com/authorize/?response_type=code&scope=user-library-modify&client_id=" . constant("SPOTIFY_CLIENT_ID") . "&redirect_uri=" . urlencode(constant("SPOTIFY_AUTH_URL"));
        header('Location: ' . $auth_url);
        
    } else {
        // code found
        $code = $_GET['code'];  

        // get the access token with curl request
        $code_url = "https://accounts.spotify.com/api/token";

        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $code_url); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    "client_id=" . constant("SPOTIFY_CLIENT_ID") 
                    . "&client_secret=" . constant("SPOTIFY_CLIENT_SECRET") 
                    . "&grant_type=authorization_code"
                    . "&code=" . $code
                    . "&redirect_uri=" . urlencode(constant("SPOTIFY_AUTH_URL")));
        $output = curl_exec($ch); 

        echo "<pre>";
        print_r(json_decode($output));
        
        curl_close($ch);   
    }
?>