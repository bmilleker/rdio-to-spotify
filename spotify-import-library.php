<?php  
    function curlGET($url) {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $r = curl_exec($ch); 
        curl_close($ch);
        
        return $r;
    }
    
    function curlPOST($url, $data, $headers) {
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $r = curl_exec($ch); 
        curl_close($ch);
        
        return $r;
    }
    
    // find the artist
    // find the albums
    // search through each album looking for a match to whats in the CSV
    // add matched album to library
    // repeat
    
    
    include_once("includes/conf.php");  
    
    $file_arr = file(constant("LIBRARY_CSV"));
    $album_added_ids = array();
    
    foreach ($file_arr as $f) {
        $csv = explode("|", $f);
        
        // find the album
        $artists = json_decode(curlGET("https://api.spotify.com/v1/search/?q=" . urlencode($csv[0]) . "&type=artist"));
        
        
        foreach ($artists->artists->items as $a) {
            // fetch artists albums
            $artist_id = $a->id;
            $artist_albums = json_decode(curlGET("https://api.spotify.com/v1/artists/" . $artist_id . "/albums"));
            $match = false;
                        
            foreach ($artist_albums->items as $aa) {
                $album_name = $aa->name;
                $album_id = $aa->id;  
                $csv_album_name = trim($csv[1]);
            
                $lev = levenshtein(trim($csv_album_name), $album_name);
                echo "{$csv_album_name} VS {$album_name}: {$lev}" . PHP_EOL; 
                
                // compare to csv record
                if ($lev <= 3) {
                    echo ">>> Match found!" . PHP_EOL;
                    $match = true;
                    $album_added_ids[] = $album_id;
                    
                    // add album to Spotify
                    $headers = array('Authorization: Bearer ' . constant("SPOTIFY_ACCESS_TOKEN"), 'Content-Type: application/json');
                    $album_added = json_decode(curlPOST("https://api.spotify.com/v1/me/albums?ids={$album_id}", false, $headers));
                    
                    print_r($album_added);
                    break; // don't keep going through the list.. duplicates will happen
                }
            }
            
            if (!$match) {
                echo "!!! No match found!" . PHP_EOL;
            }
        }
    }
?>